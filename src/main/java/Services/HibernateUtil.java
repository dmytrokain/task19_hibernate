package Services;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static SessionFactory ourSessionFactory;


    public static Session getSession() throws HibernateException {
        try {
            ourSessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ourSessionFactory.openSession(); //return opened session
    }
}
