package Services;

import Entities.PersonEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PersonService {

    private static Session session;
    List<PersonEntity> names;
    List<PersonEntity> personsList;

    public PersonService() {
        personsList = new ArrayList<PersonEntity>();
        session = HibernateUtil.getSession();
    }

    public List getNames() {
       return findAll().stream()
               .map(PersonEntity::getName)
               .collect(Collectors.toList());
    }

    public List getSurnames() {
        return findAll().stream()
                .map(PersonEntity::getSurname)
                .collect(Collectors.toList());
    }


    private List<PersonEntity> findAll() {
        List<PersonEntity> objects = new ArrayList<PersonEntity>();

        Query query = session.createQuery("from PersonEntity");

        for(Object obj: query.list()) {
            PersonEntity entity = (PersonEntity) obj;

            objects.add(entity);
        }
        return objects;
    }
}
